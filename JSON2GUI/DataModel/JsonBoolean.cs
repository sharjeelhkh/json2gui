﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NJsonSchema;
using System.Windows.Controls;
using System.Windows;

namespace JSON2GUI.DataModel
{
    class JsonBoolean : JsonRootObject
    {
        #region
        public JsonBoolean(JsonSchema4 props, string previousPath)
        {
            KeyPath = previousPath;
            JProperty = props;
        }
        #endregion

        #region Methods
        public override string GenerateJson()
        {
            base.GenerateJson();

            //string validJson = JsonConvert.SerializeObject(items);

            return "";
        }

        public override void GenerateGUI(StackPanel scp, Dictionary<string, object> outputItems)
        {
            scp.Children.Add(new CheckBox() { Content = ((JsonProperty)JProperty).Name, Width = labelWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center, FontSize = labelFont });
        }

        public override void Validate()
        {
            base.Validate();
        }
        #endregion
    }
}
