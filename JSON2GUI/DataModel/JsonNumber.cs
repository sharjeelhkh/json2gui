﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NJsonSchema;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace JSON2GUI.DataModel
{
    class JsonNumber : JsonRootObject
    {
        #region Constructors
        public JsonNumber(JsonSchema4 props, string previousPath)
        {
            KeyPath = previousPath;
            JProperty = props;
        }
        #endregion

        #region Methods
        public override string GenerateJson()
        {
            base.GenerateJson();
            return "";
        }

        public override void GenerateGUI(StackPanel scp, Dictionary<string, object> outputItems)
        {
            scp.Children.Add(new Label() { Content = ((JsonProperty)JProperty).Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont });
            Label lblValidations = new Label() { Width = labelWidth * 3, FontSize = labelFont, Foreground = Brushes.Red };

            TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
            tb.LostFocus += (sender, e) =>
            {
                //if (checkValidationsOnNumber(lblValidations, tb))
                {
                    outputItems[this.key] = tb.Text;
                }
            };
            scp.Children.Add(tb);
            scp.Children.Add(lblValidations);

        }

        public override void Validate()
        {
            base.Validate();
        }
        #endregion
    }
}
