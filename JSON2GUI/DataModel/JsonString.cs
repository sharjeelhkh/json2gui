﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NJsonSchema;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Text.RegularExpressions;

namespace JSON2GUI.DataModel
{
    class JsonString : JsonRootObject
    {
        #region Constructors
        public JsonString(JsonSchema4 props, string previousPath)
        {
            KeyPath = previousPath;
            JProperty = props;
        }
        #endregion

        #region Methods
        public override string GenerateJson()
        {
            base.GenerateJson();
            return "";
        }

        public override void GenerateGUI(StackPanel scp, Dictionary<string, object> outputItems)
        {
            scp.Children.Add(new Label() { Content = ((JsonProperty)JProperty).Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont });
            Label lblValidations = new Label() { Width = labelWidth * 3, FontSize = labelFont, Foreground = Brushes.Red };

            if (JProperty.Enumeration.Count != 0)
            {
                ComboBox cmb = new ComboBox();
                cmb.ItemsSource = JProperty.Enumeration;
                cmb.MouseLeave += (sender, e) =>
                {                    
                    outputItems[this.key] = cmb.Text;                   
                };
                scp.Children.Add(cmb);
            }
            else
            {
                TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
                tb.LostFocus += (sender, e) =>
                {
                    if (checkValidations(lblValidations, tb))
                    {
                        outputItems[this.key] = tb.Text;                        
                    }
                };
                scp.Children.Add(tb);
            }
            scp.Children.Add(lblValidations);

        }

        public override void Validate()
        {
            //base.Validate();
        }
        #endregion

        #region Local Functions
        private bool checkValidations(Label lblValidations, TextBox tb)
        {
            JsonProperty nodeJson = (JsonProperty)JProperty;
            DateTime dt;
            if (tb.GetLineLength(0) < (nodeJson.MinLength == null ? 0 : nodeJson.MinLength))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to MinLength";
                //tb.Focus();
            }
            else if (tb.GetLineLength(0) > (nodeJson.MaxLength == null ? tb.GetLineLength(0) : nodeJson.MaxLength))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to MaxLength";
                //tb.Focus();
            }
            else if (nodeJson.Format == "date-time" && !(DateTime.TryParse(tb.Text, out dt)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid Date Time";
            }
            else if (nodeJson.Format == "email" && !(IsValidEmailAddress(tb.Text)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid Email Address";
            }
            else if (nodeJson.Format == "hostname" && !(IsValidHostName(tb.Text)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid Host Name";
            }
            else if ((nodeJson.Format == "ipv6" || nodeJson.Format == "ipv4") && !(IsValidIPAddress(tb.Text)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid IP Address";
            }
            else if ((nodeJson.Format == "uri" || nodeJson.Format == "uri-reference" || nodeJson.Format == "uri-template") && !(Uri.IsWellFormedUriString(tb.Text, UriKind.RelativeOrAbsolute)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid URI";
            }
            /* else if (!nodeJson.Enumeration.Contains(tb.Text))
             {
                 lblValidations.Content = "!" + " Value doesn't Conforms to  Enumerations";
                 lblMessage.Content = "Value doesn't Conforms to Enumerations";
             }*/
            else
            {
                lblValidations.Content = "";
                return true;
            }

            return false;
        }

        public static bool IsValidEmailAddress(string emailaddress)
        {
            try
            {
                Regex rx = new Regex(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
                return rx.IsMatch(emailaddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsValidIPAddress(string ipaddress)
        {
            try
            {
                Regex rx = new Regex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
                return rx.IsMatch(ipaddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsValidHostName(string hostName)
        {
            try
            {
                Regex rx = new Regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9]*[a-zA-Z0-9]))*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9]*[A-Za-z0-9])$");
                return rx.IsMatch(hostName);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        #endregion
    }

    public enum stringFormat
    {
        date_time,
        email,
        hostname,
        ipv4,
        ipv6,
        uri        
    }
}
