﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NJsonSchema;
using System.Windows.Controls;

namespace JSON2GUI.DataModel
{
    class JsonRootObject
    {
        public int labelWidth = 110, labelFont = 14;
        public int fieldWidth = 100;

        public string schemaDraft { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        
        public string id { get; set; }

        public string key { get; set; }
        public string value { get; set; }

        private JsonSchema4 jProperies;
        public JsonSchema4 JProperty
        {
            get
            {
                return jProperies;
            }

            set
            {
                jProperies = value;
            }
        }

        private string keyPath;
        public string KeyPath
        {
            get
            {
                return keyPath;
            }

            set
            {
                keyPath = value;
            }
        }

        public virtual string GenerateJson()
        {
            return "";
        }

        public virtual void Validate()
        { }

        public virtual void GenerateGUI(StackPanel scp, Dictionary<string, object> outputItems)
        { }

    }
}
