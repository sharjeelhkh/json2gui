﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NJsonSchema;
using Newtonsoft.Json;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace JSON2GUI.DataModel
{
    class JsonInteger : JsonRootObject
    {
        #region Constructors
        public JsonInteger(JsonSchema4 props, string previousPath)
        {
            KeyPath = previousPath;
            JProperty = props;
        }
        #endregion

        #region Methods
        public override string GenerateJson()
        {
           // base.GenerateJson();
            //string validJson = JsonConvert.SerializeObject(items);

            return "";
        }

        public override void GenerateGUI(StackPanel scp, Dictionary<string, object> outputItems)
        {
            scp.Children.Add(new Label() { Content = ((JsonProperty)JProperty).Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont });
            Label lblValidations = new Label() { Width = labelWidth * 3, FontSize = labelFont, Foreground = Brushes.Red };

            TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
            tb.LostFocus += (sender, e) =>
            {
                if (checkValidations(lblValidations, tb))
                {
                    outputItems[this.key] = tb.Text;
                }
            };
            scp.Children.Add(tb);
            scp.Children.Add(lblValidations);            
        }

        public override void Validate()
        {
            base.Validate();
        }
        #endregion

        #region Local Functions
        private bool checkValidations(Label lblValidations, TextBox tb)
        {
            JsonProperty nodeJson = (JsonProperty)JProperty;
            int temp;
            if (tb.Text == null || tb.Text == "")
                return false;

            if (!int.TryParse(tb.Text, out temp))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to Integer Input";
                tb.Text = "";
            }
            else if (Int32.Parse(tb.Text) < (nodeJson.Minimum == null ? 0 : nodeJson.Minimum))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to Minimum";
                //tb.Focus();
            }
            else if (Int32.Parse(tb.Text) > (nodeJson.Maximum == null ? Int32.Parse(tb.Text) : nodeJson.Maximum))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to Maximum";
                //tb.Focus();
            }
            else if (nodeJson.IsExclusiveMinimum)
            {
                if (Int32.Parse(tb.Text) <= (nodeJson.Minimum == null ? 0 : nodeJson.Minimum))
                {
                    lblValidations.Content = "!" + " Value doesn't Conforms to Exclusive Minimum";
                    //tb.Focus();
                }
                else if (Int32.Parse(tb.Text) >= (nodeJson.Maximum == null ? Int32.Parse(tb.Text) : nodeJson.Maximum))
                {
                    lblValidations.Content = "!" + " Value doesn't Conforms to Exclusive Maximum";
                    //tb.Focus();
                }
            }
            else if (Int32.Parse(tb.Text) % (nodeJson.MultipleOf == null ? 1 : nodeJson.MultipleOf) != 0)        //nodeJson.MultipleOf != 0)
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to MultipleOf";
                //tb.Focus();
            }
            else
            {
                lblValidations.Content = "";
                return true;
            }
            return false;
        }

        #endregion
    }
}
