﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NJsonSchema;
using Newtonsoft.Json;
using System.Windows.Controls;

namespace JSON2GUI.DataModel
{
    class JsonObject : JsonRootObject
    {        
        Dictionary<String, JsonRootObject> items;
        internal Dictionary<string, JsonRootObject> Items
        {
            get
            {
                return items;
            }

            set
            {
                items = value;
            }
        }

        Dictionary<string, object> outputItems;
        public Dictionary<string, object> OutputItems
        {
            get
            {
                return outputItems;
            }

            set
            {
                outputItems = value;
            }
        }

        #region Constructors

        public JsonObject(JsonSchema4 childProps, string previousPath)
        {          
            KeyPath = previousPath;
            JProperty = childProps;

            items = new Dictionary<string, JsonRootObject>();
            outputItems = new Dictionary<string, object>();

            foreach (var node in childProps.Properties)
            {
                JsonSchema4 nodeJson = node.Value;
                switch (nodeJson.Type)
                {
                    case JsonObjectType.Boolean:
                        {
                            JsonBoolean jBoolean = new JsonBoolean(nodeJson, KeyPath + "/" + node.Key);
                            jBoolean.key = node.Key;

                            items.Add(node.Key, jBoolean);
                            outputItems.Add(node.Key, false);
                            break;
                        }
                    case JsonObjectType.String:
                        {
                            JsonString jString = new JsonString(nodeJson, KeyPath + "/" + node.Key);
                            jString.key = node.Key;

                            items.Add(node.Key, jString);
                            outputItems.Add(node.Key, "");
                            break;
                        }
                    case JsonObjectType.Integer:
                        {
                            JsonInteger jInteger = new JsonInteger(nodeJson, KeyPath + "/" + node.Key);
                            jInteger.key = node.Key;

                            items.Add(node.Key, jInteger);
                            outputItems.Add(node.Key, 0);
                            break;
                        }
                    case JsonObjectType.Number:
                        {
                            JsonNumber jNumber = new JsonNumber(nodeJson, KeyPath + "/" + node.Key);
                            jNumber.key = node.Key;

                            items.Add(node.Key, jNumber);
                            outputItems.Add(node.Key, 0.0);
                            break;
                        }
                    case JsonObjectType.Array:
                        {
                            JsonArray jArray = new JsonArray(nodeJson, KeyPath + "/" + node.Key);
                            jArray.key = node.Key;

                            items.Add(node.Key, jArray);
                            //outputItems.Add(node.Key, jArray.OutputItems);
                            outputItems.Add(node.Key, jArray.OutputItems.Values.ToArray());
                            break;
                        }
                    case JsonObjectType.Object:
                        {
                            JsonObject jObj = new JsonObject(nodeJson, KeyPath + "/" + node.Key);
                            jObj.key = node.Key;

                            items.Add(node.Key, jObj);
                            outputItems.Add(node.Key, jObj.outputItems);
                            break;
                        }
                }
            }
        }

        #endregion

        #region Methods
        public override string GenerateJson()
        {
                                
            string validJson = JsonConvert.SerializeObject(outputItems);
            
            return validJson;
        }

        public virtual void GenerateGUI(StackPanel scp)
        {
            StackPanel arrStack = new StackPanel() { Orientation = Orientation.Vertical };

            foreach (var node in Items)
            {
                StackPanel nodeStack = new StackPanel() { Orientation = Orientation.Horizontal };
               
                switch (node.Value.JProperty.Type)
                {
                    case JsonObjectType.Boolean:
                        {
                            JsonBoolean jBoolean = (JsonBoolean)node.Value;
                            jBoolean.GenerateGUI(nodeStack, outputItems);
                            break;
                        }
                    case JsonObjectType.String:
                        {
                            JsonString jString = (JsonString)node.Value;
                            
                            jString.GenerateGUI(nodeStack, outputItems);
                            break;
                        }
                    case JsonObjectType.Integer:
                        {
                            JsonInteger jInteger = (JsonInteger)node.Value;
                            jInteger.GenerateGUI(nodeStack, outputItems);
                            break;
                        }
                    case JsonObjectType.Number:
                        {
                            JsonNumber jNumber = (JsonNumber)node.Value;
                            jNumber.GenerateGUI(nodeStack, outputItems);
                            break;
                        }
                    case JsonObjectType.Array:
                        {
                            JsonArray jArray = (JsonArray)node.Value;
                            jArray.GenerateGUI(nodeStack);
                            break;
                        }
                    case JsonObjectType.Object:
                        {
                            JsonObject jObject = (JsonObject)node.Value;
                            jObject.GenerateGUI(nodeStack);
                            break;
                        }
                }

                arrStack.Children.Add(nodeStack);
            }

            scp.Children.Add(arrStack);
        }

        public override void Validate()
        {
            base.Validate();
        }
        #endregion
    }
}
