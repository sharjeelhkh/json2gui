﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using NJsonSchema;
using JSON2GUI.DataModel;
using System.Windows.Controls;

namespace JSON2GUI
{
    class JsonManager
    {
        #region Fields
        string filename;
        string[] firstObject;
        JsonSchema4 schema;
        Dictionary<string, JsonRootObject> dictOfNode;
        #endregion

        #region Properties
        public string Filename
        {
            get
            {
                return filename;
            }

            set
            {
                filename = value;
            }
        }
        Dictionary<string, object> outputDict;
        public Dictionary<string, object> OutputDict
        {
            get
            {
                return outputDict;
            }

            set
            {
                outputDict = value;
            }
        }

        #endregion

        #region Delegates
        public EventHandler onSchemaLoaded;

        #endregion

        #region Constructors
        public JsonManager(string filename)
        {
            if (filename != null)
            {
                ReadFile(filename);
                dictOfNode = new Dictionary<string, JsonRootObject>();                
            }
        }
        #endregion

        #region Methods
        private async void ReadFile(string filename)
        {
            schema = await JsonSchema4.FromFileAsync(filename + ".json");

            var schemaData = schema.ToJson();
            var errors = schema.Validate(schemaData);

            foreach (var error in errors)
                Console.WriteLine(error.Path + ": " + error.Kind);

            //schema = await JsonSchema4.FromJsonAsync(schemaData);

            if (schema != null)
            {
                firstObject = new string[schema.Properties.Count];
                ParseSchema();
                onSchemaLoaded(this, null);
            }
        }

        public void ParseSchema()
        {
            int i = 0;
            foreach (var node in schema.Properties)
            {
                JsonSchema4 nodeJson = node.Value;
                switch (nodeJson.Type)
                {
                    case JsonObjectType.Boolean:
                        {
                            JsonBoolean jBoolean = new JsonBoolean(nodeJson, node.Key);

                            dictOfNode.Add(node.Key, jBoolean);
                            break;
                        }
                    case JsonObjectType.String:
                        {
                            JsonString jString = new JsonString(nodeJson, node.Key);

                            dictOfNode.Add(node.Key, jString);
                            break;
                        }
                    case JsonObjectType.Integer:
                        {
                            JsonInteger jInteger = new JsonInteger(nodeJson, node.Key);

                            dictOfNode.Add(node.Key, jInteger);
                            break;
                        }
                    case JsonObjectType.Number:
                        {
                            JsonInteger jNumber = new JsonInteger(nodeJson, node.Key);

                            dictOfNode.Add(node.Key, jNumber);
                            break;
                        }
                    case JsonObjectType.Array:
                        {
                            JsonArray jArray = new JsonArray(nodeJson, node.Key);                         

                            dictOfNode.Add(node.Key, jArray);
                            break;
                        }
                    case JsonObjectType.Object:
                        {
                            JsonObject jObj = new JsonObject(nodeJson, node.Key);
                            //jObj.KeyPath = node.Key;

                            firstObject[i] = node.Key;
                            dictOfNode.Add(node.Key, jObj);
                            break;
                        }
                }
                i++;
            }           
        }

        public string GenerateValidJson()
        {
            string validJson = "{";
            int i = 0;
            foreach (JsonRootObject node in dictOfNode.Values)
            {
                if (validJson != "{")
                    validJson = validJson + ",";

                validJson = validJson + JsonConvert.SerializeObject(schema.Properties[firstObject[i]].Name) + ":";
                validJson = validJson + node.GenerateJson();
                i++;
            }
            validJson = validJson + "}";
            return validJson;
        }

        public StackPanel GenerateGUI()
        {
            StackPanel maindStack = new StackPanel() { Orientation = Orientation.Vertical };

            foreach (KeyValuePair<string, JsonRootObject> node in dictOfNode)
            {
                StackPanel nodeStack = new StackPanel() { Orientation = Orientation.Horizontal };
            
                switch (node.Value.JProperty.Type)
                {
                    case JsonObjectType.Boolean:
                        {
                            JsonBoolean jBoolean = (JsonBoolean)node.Value;
                            jBoolean.GenerateGUI(nodeStack, outputDict);                            
                            break;
                        }
                    case JsonObjectType.String:
                        {
                            JsonString jString = (JsonString)node.Value;
                            jString.GenerateGUI(nodeStack, outputDict);
                            break;
                        }
                    case JsonObjectType.Integer:
                        {
                            JsonInteger jInteger = (JsonInteger)node.Value;
                            jInteger.GenerateGUI(nodeStack, outputDict);
                            break;
                        }
                    case JsonObjectType.Number:
                        {
                            JsonNumber jNumber = (JsonNumber)node.Value;
                            jNumber.GenerateGUI(nodeStack, outputDict);
                            break;
                        }
                    case JsonObjectType.Array:
                        {
                            JsonArray jArray = (JsonArray)node.Value;
                            jArray.GenerateGUI(nodeStack);
                            break;
                        }
                    case JsonObjectType.Object:
                        {
                            JsonObject jObject = (JsonObject)node.Value;
                            jObject.GenerateGUI(nodeStack);
                            break;
                        }
                }
                
                maindStack.Children.Add(nodeStack);
            }
           
            return maindStack;
        }
        #endregion
    }
}
