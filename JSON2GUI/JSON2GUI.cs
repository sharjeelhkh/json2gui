﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Newtonsoft.Json;
using NJsonSchema;
using System.Text.RegularExpressions;
using JSON2GUI.DataModel;
//using NJsonSchema.CodeGeneration;
//using NJsonSchema.CodeGeneration.CSharp;

namespace JSON2GUI
{
    public class json2gui
    {
        #region Fields
        JsonSchema4 schema;
        IDictionary<string, JsonProperty> jsonDict;
        string jsonString;

        string filename;
        int labelWidth = 110, labelFont=14;
        int fieldWidth = 100;
        #endregion

        #region Properties
        public string JsonString
        {
            get
            {
                return jsonString;
            }

            set
            {
                jsonString = value;
            }
        }

        public IDictionary<string, JsonProperty> JsonDict
        {
            get
            {
                return jsonDict;
            }

            set
            {
                jsonDict = value;
            }
        }

        #endregion

        #region Delegates
        public EventHandler onSchemaLoaded;

        #endregion

        #region Constructors
        public json2gui(string filename)
        {
            this.filename = filename;
            ReadFile();           
        }
        #endregion

        #region Methods
        private async void ReadFile()
        {
            schema = await JsonSchema4.FromFileAsync(filename + ".json");

            var schemaData = schema.ToJson();    schemaData.Insert(0, "test");        
            var errors = schema.Validate(schemaData);

            foreach (var error in errors)
                Console.WriteLine(error.Path + ": " + error.Kind);

            schema = await JsonSchema4.FromJsonAsync(schemaData);

            if (schema != null)
            {
                JsonDict = schema.Properties[filename].Properties;

                onSchemaLoaded(this, null);
            }
        }

        public void GenerateGUI(IDictionary<string, JsonProperty> jsonDict, StackPanel scp)
        {                    
            #region Setting RootObject Info into DataModel 
            /*JsonRootObject jRootObj = new JsonRootObject();
            jRootObj.type = (JsonType)Enum.Parse(typeof(JsonType), schema.Type.ToString());
            jRootObj.id = schema.Id;
            jRootObj.schemaDraft = schema.SchemaVersion;
            jRootObj.title = schema.Title;
            jRootObj.description = schema.Description;
            jInstance.rootObject = jRootObj;

            if (schema.Type == JsonObjectType.Object)
            {
                JsonObject jObject = new JsonObject();
                jObject.type = JsonType.Object;
                jObject.id = schema.Properties[filename].Id;
                jInstance.Object = jObject;
            }*/
            #endregion          
          
            Dictionary<string, object> desiredResult = new Dictionary<string, object>();

            foreach (object node in jsonDict.Values)
            {
                NJsonSchema.JsonProperty nodeJson = (NJsonSchema.JsonProperty)node;                
                StackPanel scpInner = new StackPanel() { Orientation = Orientation.Horizontal };

                switch (nodeJson.Type)
                {
                    case JsonObjectType.Boolean:
                        {
                            scpInner.Children.Add(new CheckBox() { Content = nodeJson.Name, Width = labelWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center, FontSize = labelFont });
                            desiredResult.Add(nodeJson.Id, false);
                            break;
                        }
                    case JsonObjectType.String:
                        {
                            scpInner.Children.Add(new Label() { Content = nodeJson.Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont });
                            Label lblValidations = new Label() { Width = labelWidth * 3, FontSize = labelFont, Foreground = Brushes.Red };

                            if (nodeJson.Enumeration.Count != 0)
                            {
                                ComboBox cmb = new ComboBox();
                                cmb.ItemsSource = nodeJson.Enumeration;
                                cmb.MouseLeave += (sender, e) =>
                                {
                                    desiredResult[nodeJson.Id] = cmb.Text;
                                    ExtractJsonString(desiredResult);
                                };
                                scpInner.Children.Add(cmb);
                            }
                            else
                            {
                                TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
                                tb.LostFocus += (sender, e) =>
                                {
                                    if (checkValidationsOnString(nodeJson, lblValidations, tb))
                                    {
                                        //jInstance.String.value = tb.Text;
                                        desiredResult[nodeJson.Id] = tb.Text;
                                        ExtractJsonString(desiredResult);
                                    }
                                };
                                scpInner.Children.Add(tb);
                            }
                            scpInner.Children.Add(lblValidations);

                            desiredResult.Add(nodeJson.Id, "");
                            break;
                        }
                    case JsonObjectType.Integer:
                        {
                            scpInner.Children.Add(new Label() { Content = nodeJson.Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont });
                            Label lblValidations = new Label() { Width = labelWidth * 3, FontSize = labelFont, Foreground = Brushes.Red };

                            TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
                            tb.LostFocus += (sender, e) =>
                            {
                                if(checkValidationsOnInteger( nodeJson, lblValidations, tb))
                                {
                                    desiredResult[nodeJson.Id] = tb.Text;
                                    ExtractJsonString(desiredResult);
                                }
                            };
                            scpInner.Children.Add(tb);
                            scpInner.Children.Add(lblValidations);

                            desiredResult.Add(nodeJson.Id, 0);
                            break;
                        }
                    case JsonObjectType.Array:
                        {
                            scpInner.Children.Add(new Label() { Content = nodeJson.Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont });
                            int i = 0;

                            //GenerateGUI(nodeJson.Item.Properties, scpInner);

                            #region Inner Objects into Array
                            
                            Dictionary<string, object> arrOutput = new Dictionary<string, object>();
                            foreach (object item in nodeJson.Item.ActualProperties.Values)
                            {
                                StackPanel scpArray = new StackPanel() { Orientation = Orientation.Vertical };
                                NJsonSchema.JsonProperty itemJson = (NJsonSchema.JsonProperty)item;                              

                                scpArray.Children.Add(new Label() { Content = itemJson.Name, Width = labelWidth, HorizontalAlignment = HorizontalAlignment.Left, FontSize = labelFont - 2 });
                                //scpArray.Children.Add(new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center, Background = Brushes.OldLace });                                                                

                                Label lblValidations = new Label() { Width = labelWidth * 3, FontSize = labelFont, Foreground = Brushes.Red };
                                TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center, Background = Brushes.OldLace };
                                tb.LostFocus += (sender, e) =>
                                {
                                    if (checkValidationsOnInteger(nodeJson, lblValidations, tb))
                                    {
                                        arrOutput[itemJson.Id] = tb.Text;
                                        desiredResult[nodeJson.Id] = arrOutput;

                                        ExtractJsonString(desiredResult);
                                    }
                                };
                                scpArray.Children.Add(tb);
                                scpArray.Children.Add(lblValidations);

                                //Need to add dictionary of object here into arrOutput                                                                
                                arrOutput.Add(itemJson.Id, "");
                                scpInner.Children.Add(scpArray);
                                i++;
                            }                            
                            desiredResult.Add(nodeJson.Id, arrOutput);
                            
                            #endregion
                            break;
                        }
                }
                scp.Children.Add(scpInner);
            }

            ExtractJsonString(desiredResult);

            scp.Children.Add(new TextBox() { Text = jsonString });
            //return scp;
        }

        private void ExtractJsonString(Dictionary<string, object> desiredResult)
        {
            this.jsonString = JsonConvert.SerializeObject(desiredResult);
        }

        private void InsertData()
        {
            schema.Properties[filename].Properties["brand"].Description = "GlokoSmithKlien";
            var schemaData = schema.ToJson();
        }

        #endregion

        #region Local Functions
        private bool checkValidationsOnString(JsonProperty nodeJson, Label lblValidations, TextBox tb)
        {
            DateTime dt;
            if (tb.GetLineLength(0) < (nodeJson.MinLength == null ? 0 : nodeJson.MinLength))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to MinLength";
                //tb.Focus();
            }
            else if (tb.GetLineLength(0) > (nodeJson.MaxLength == null ? tb.GetLineLength(0) : nodeJson.MaxLength))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to MaxLength";
                //tb.Focus();
            }
            else if (nodeJson.Format == "date-time" && !(DateTime.TryParse(tb.Text, out dt)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid Date Time";
            }
            else if (nodeJson.Format == "email" && !(IsValidEmailAddress(tb.Text)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid Email Address";
            }
            else if (nodeJson.Format == "hostname" && !(IsValidHostName(tb.Text)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid Host Name";
            }
            else if ((nodeJson.Format == "ipv6" || nodeJson.Format == "ipv4") && !(IsValidIPAddress(tb.Text)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid IP Address";
            }
            else if ((nodeJson.Format == "uri" || nodeJson.Format == "uri-reference" || nodeJson.Format == "uri-template") && !(Uri.IsWellFormedUriString(tb.Text, UriKind.RelativeOrAbsolute)))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to valid URI";
            }
           /* else if (!nodeJson.Enumeration.Contains(tb.Text))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to  Enumerations";
                lblMessage.Content = "Value doesn't Conforms to Enumerations";
            }*/
            else
            {
                lblValidations.Content = "";
                return true;
            }

            return false;
        }

        private static bool checkValidationsOnInteger(JsonProperty nodeJson, Label lblValidations, TextBox tb)
        {
            int temp;
            if (tb.Text == null || tb.Text == "")
                return false;

            if (!int.TryParse(tb.Text, out temp))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to Integer Input";
                tb.Text = "";          
            }
            else if (Int32.Parse(tb.Text) < (nodeJson.Minimum == null ? 0 : nodeJson.Minimum))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to Minimum";
                //tb.Focus();
            }
            else if (Int32.Parse(tb.Text) > (nodeJson.Maximum == null ? Int32.Parse(tb.Text) : nodeJson.Maximum))
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to Maximum";
                //tb.Focus();
            }
            else if(nodeJson.IsExclusiveMinimum)
            {
                if (Int32.Parse(tb.Text) <= (nodeJson.Minimum == null ? 0 : nodeJson.Minimum))
                {
                    lblValidations.Content = "!" + " Value doesn't Conforms to Exclusive Minimum";
                    //tb.Focus();
                }
                else if (Int32.Parse(tb.Text) >= (nodeJson.Maximum == null ? Int32.Parse(tb.Text) : nodeJson.Maximum))
                {
                    lblValidations.Content = "!" + " Value doesn't Conforms to Exclusive Maximum";
                    //tb.Focus();
                }
            }
            else if (Int32.Parse(tb.Text) % (nodeJson.MultipleOf == null ? 1 : nodeJson.MultipleOf) != 0)        //nodeJson.MultipleOf != 0)
            {
                lblValidations.Content = "!" + " Value doesn't Conforms to MultipleOf";
                //tb.Focus();
            }
            else
            {
                lblValidations.Content = "";
                return true;
            }
            return false;
        }

        public static bool IsValidEmailAddress(string emailaddress)
        {
            try
            {
                Regex rx = new Regex(@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
                return rx.IsMatch(emailaddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsValidIPAddress(string ipaddress)
        {
            try
            {
                Regex rx = new Regex("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
                return rx.IsMatch(ipaddress);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsValidHostName(string hostName)
        {
            try
            {
                Regex rx = new Regex("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9]*[a-zA-Z0-9]))*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9]*[A-Za-z0-9])$");
                return rx.IsMatch(hostName);
            }
            catch (FormatException)
            {
                return false;
            }
        }
        #endregion

        #region Older Functions
        private TextBox AddStringField()
        {
            TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
            tb.LostFocus += stringField_LostFocus;
            return tb;
        }

        private void stringField_LostFocus(object sender, RoutedEventArgs e)
        {
            //if(tb.GetLineLength(0) < currentJsonItem.MinLength)
            {
                MessageBox.Show("Entry too Short");
            }
        }

        private TextBox AddIntegerField()
        {
            TextBox tb = new TextBox() { Width = fieldWidth, Height = 20, HorizontalAlignment = HorizontalAlignment.Center };
            tb.LostFocus += integerField_LostFocus;
            return tb;
        }

        private void integerField_LostFocus(object sender, RoutedEventArgs e)
        {
            //if(currentJsonItem)
        }

        #endregion

    }


}
