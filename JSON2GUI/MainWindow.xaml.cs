﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JSON2GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        json2gui j2g;

        JsonManager jManager;

        public MainWindow()
        {
            InitializeComponent();
            //j2g = new json2gui("cgm");
            //j2g.onSchemaLoaded += (object sender, EventArgs e) => makeViewFromSchema();

            jManager = new JsonManager("cgm");
            jManager.onSchemaLoaded += (object sender, EventArgs e) => makeViewFromSchema();
        }

        private void makeViewFromSchema()
        {
            //this.mainStack.Children.Add(j2g.GenerateGUI(j2g.JsonDict));
            //j2g.GenerateGUI(j2g.JsonDict, mainStack);

            mainStack.Children.Add(jManager.GenerateGUI());
        }

        private void btnJson_Click(object sender, RoutedEventArgs e)
        {
            //txtJson.Text = j2g.JsonString;
            txtJson.Text = jManager.GenerateValidJson();

        }
    }
}
